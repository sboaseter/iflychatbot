﻿using Hangfire;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iFlyChatBot.UI.Controllers
{
    public class BotController : Controller
    {
        // GET: Bot
        public ActionResult Start()
        {
            //if(!iFlyChatBot.Running) 
            RecurringJob.AddOrUpdate(() => MvcApplication.bot.Update(), Cron.Minutely);
            //MvcApplication.bot.Update();

            iFlyChatBot.Running = true;
            return RedirectToAction("Index", "Admin");
//            return View("~/Views/Admin/Index");
        }
        public ActionResult Stop()
        {
            RecurringJob.RemoveIfExists("botproc");            
            iFlyChatBot.Running = false;
            return RedirectToAction("Index", "Admin");
            //return View("~/Views/Admin/Index");
        }
        public ActionResult Configure()
        {
            return RedirectToAction("Index", "Admin");
//            return View("~/Views/Admin/Index");
        }

        public ActionResult SendMessage(int UserId, int SiteId, string message) {
            iFlyChatBot.PostFromDashboard(SiteId, UserId, message);
            return RedirectToAction("Users", "Admin");
        }
    }
}