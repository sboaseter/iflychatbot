﻿using Hangfire;
using iFlyChatBot.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace iFlyChatBot.UI.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            //RecurringJob.AddOrUpdate(() => Console.Write("Easy!"), Cron.Minutely);
            //RecurringJob.AddOrUpdate(() => Debug.WriteLine("Easy!"), Cron.Minutely);
            return View();
        }

        public ActionResult SiteUser(int id, int uid)
        {
            iFlyChatBot.SiteUserUpdate(id, uid);
            return RedirectToAction("Index");
        }

        public ActionResult Sites()
        {
            return View();
        }
        public ActionResult SiteDetail(int id = -1)
        {
            if (id == -1) return View(new iFlyChat_Site());
            return View(Config.Sites.Find(x => x.Id == id));
        }

        [HttpPost]
        public ActionResult SaveSite(iFlyChat_Site model)
        {
            iFlyChatBot.SaveSite(model);
            return RedirectToAction("Sites");
        }

        public ActionResult DeleteSite(int id)
        {
            iFlyChatBot.DeleteSite(id);
            return RedirectToAction("Sites");
        }

        public ActionResult Users()
        {
            return View();
        }
        public ActionResult UserDetail(int id = -1)
        {
            if (id == -1) return View(new iFlyChat_User());
            return View(Config.Users.Find(x => x.Id == id));
        }

        [HttpPost]
        public ActionResult SaveUser(iFlyChat_User model)
        {
            iFlyChatBot.SaveUser(model);
            return RedirectToAction("Users");
        }

        public ActionResult DeleteUser(int id)
        {
            iFlyChatBot.DeleteUser(id);
            return RedirectToAction("Users");
        }

        public ActionResult Sources()
        {
            return View();
        }

        public ActionResult SourceDetail(int id = -1)
        {
            if (id == -1) return View(new iFlyChat_Source());
            return View(Config.Sources.Find(x => x.Id == id));
        }
        
        [HttpPost]
        public ActionResult SaveSource(iFlyChat_Source model)
        {
            iFlyChatBot.SaveSource(model);
            return RedirectToAction("Sources");
        }
        public ActionResult DeleteSource(int id)
        {
            iFlyChatBot.DeleteSource(id);
            return RedirectToAction("Sources");
        }

        public ActionResult Readers() {
            return View();
        }

        public ActionResult ReaderDetail(int id = -1) {
            if (id == -1) return View(new iFlyChat_Reader_Twitter());
            return View(Config.TwtterReaders.Find(x => x.Id == id));
        }

        [HttpPost]
        public ActionResult SaveReader(iFlyChat_Reader_Twitter model) {
            iFlyChatBot.SaveReader(model);
            return RedirectToAction("Readers");
        }
        public ActionResult DeleteReader(int id) {
            iFlyChatBot.DeleteReader(id);
            return RedirectToAction("Readers");
        }

    }
}