﻿using Hangfire;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Routing;

namespace iFlyChatBot.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
        public static iFlyChatBot bot;

        
        protected void Application_Start()
        {
            XmlConfigurator.Configure();
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            Log.Info("iFlyBotUI v0.1 Starting.");
            Log.InfoFormat("Assembly   : {0}, {1}", Assembly.GetExecutingAssembly().FullName, Assembly.GetCallingAssembly().GetName().Version);

            try
            {
                var connStr = ConfigurationManager.ConnectionStrings["iFlyChatBotDB"].ConnectionString;
                bot = new iFlyChatBot(connStr);
                Log.InfoFormat("Total Sites: {0}", Config.Sites.Count);
                Log.InfoFormat("Sources        : {0}", Config.Sources.Count);
                foreach (var s in Config.Sites)
                {
                    Log.InfoFormat("Site        : {0}", s.Name);
                    Log.InfoFormat("Users        : {0}", s.Users.Count);
                }
            }
            catch (Exception ex)
            {
                Log.ErrorFormat("Error occured reading required variables!: {0}", ex.ToString());
            }
        }
    }
}
