﻿using Hangfire;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(iFlyChatBot.UI.Startup))]

namespace iFlyChatBot.UI
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration
                .UseSqlServerStorage("Hangfire");
            
            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                AuthorizationFilters = new[] { new iFlyRestrictiveAuthorizationFilter() }
            });
            app.UseHangfireServer();
        }
    }
}