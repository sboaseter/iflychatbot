﻿using iFlyChatBot.Models;
using System.Collections.Generic;

namespace iFlyChatBot
{
    public static class Config
    {
        public static string ConnStr { get; set; }
        public static List<iFlyChat_Site> Sites;
        public static List<iFlyChat_User> Users;
        public static List<iFlyChat_Source> Sources;
        public static List<iFlyChat_Site_User> SitesUsers;

        public static List<iFlyChat_Reader_Twitter> TwtterReaders;

        public static List<iFlyChat_Message> Messages;

    }
}
