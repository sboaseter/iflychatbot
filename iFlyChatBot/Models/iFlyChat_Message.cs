﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iFlyChatBot.Models {
    public class iFlyChat_Message {
        public int Id { get; set; }
        public int SourceId { get; set; }
        public int ReaderId { get; set; }
        public int? UserId { get; set; }
        public string Text { get; set; }
        public string Raw { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime PostedOn { get; set; }

    }
}

