﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iFlyChatBot.Models
{
    public class iFlyChat_Site_User
    {
        public int Id { get; set; }
        public int SiteId { get; set; }
        public int UserId { get; set; }
    }
}
