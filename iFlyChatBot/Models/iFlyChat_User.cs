﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iFlyChatBot.Models
{
    public class iFlyChat_User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public string Image { get; set; }
        public int SourceId { get; set; }
        public int FlyChatId { get; set; }

    }
}
