﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iFlyChatBot.Models
{
    class iFlyChat_Publish
    {
        public string api_key { get; set; }
        public string uid { get; set; }
        public string name { get; set; }
        public string picture_url { get; set; }
        public string profile_url { get; set; }
        public string message { get; set; }
        public string color { get; set; }
        public string roles { get; set; }
    }
}
