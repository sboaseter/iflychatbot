﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iFlyChatBot.Models
{
    public class iFlyChat_Source
    {
        public int Id { get; set; }

        public int ReaderId { get; set; }

        public string Username { get; set; }
        public Int64 SinceId { get; set; }
        
        public DateTime LastUpdate { get; set; }

        public List<iFlyChat_Message> Messages { get; set; }
    }
}
