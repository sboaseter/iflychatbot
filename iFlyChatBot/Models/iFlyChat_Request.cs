﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iFlyChatBot.Models {
    public class iFlyChat_Request {
        public int Id { get; set; }
        public int ReaderId { get; set; }
        public int SourceId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Message { get; set; }
    }
}
