﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iFlyChatBot.Models {
    public interface IiFlyChat_Reader {
        int Id { get; set; }
        string Type { get; set; }
        DateTime LastUpdate { get; set; }
        string Username { get; set; }
    }
}
