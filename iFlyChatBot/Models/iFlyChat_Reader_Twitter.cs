﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace iFlyChatBot.Models {
    public class iFlyChat_Reader_Twitter : IiFlyChat_Reader {
        public int Id { get; set; }
        public string Type { get; set; }
        public DateTime LastUpdate { get; set; }
        public string Username { get; set; }

        public string Password { get; set; }
        public string twitter_accessToken { get; set; }
        public string twitter_accessTokenSecret { get; set; }
        public string twitter_consumerKey { get; set; }
        public string twitter_consumerSecret { get; set; }

        public List<iFlyChat_Source> GetSources() {
            return Config.Sources.FindAll(x => x.ReaderId == Id);
        }


        public List<iFlyChat_Message> GetMessages() {
            return Config.Messages.FindAll(x => x.ReaderId == Id && x.UserId.HasValue).OrderByDescending(x => x.CreatedOn).ToList();
        }

        // in iflyBot.cs instead
//        public int Requests(int minutes) {


//        }

    }
}
