﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace iFlyChatBot.Models
{
    public class iFlyChat_Site
    {
        public iFlyChat_Site()
        {
            this.Users = new List<iFlyChat_User>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string ApiKey { get; set; }
        public string PublishUrl { get; set; }
        public string RoomId { get; set; }

        public List<iFlyChat_User> Users { get; set; }
    }
}
