﻿using iFlyChatBot.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.XPath;
using TwitterClient;
using Dapper;
namespace iFlyChatBot {
    public class iFlyChatBot {
        #region log4net
        private static readonly log4net.ILog Log = log4net.LogManager.GetLogger(
            System.Reflection.MethodBase.GetCurrentMethod().DeclaringType
        );
        #endregion
        public static bool Running { get; set; }
        public API api;

        public iFlyChatBot() {
        }
        public iFlyChatBot(string connStr) {
            Running = false;
            Config.ConnStr = connStr;
            try {
                Initialize();
                Running = true;
            } catch (Exception ex) {
                Log.ErrorFormat("Error initializing with: {0}", connStr);
            }
        }

        public static string Hash(string stringToHash) {
            using (var sha1 = new SHA1Managed()) {
                return BitConverter.ToString(sha1.ComputeHash(Encoding.UTF8.GetBytes(stringToHash)));
            }
        }

        public static void DeleteSite(int id) {
            using (var conn = new SqlConnection(Config.ConnStr)) {
                if (Config.Sites.Any(x => x.Id == id)) {
                    //Delete
                    conn.Query(@"delete from [Site] where [Id]=@Id", new { Id = id });
                }
                Config.Sites = conn.Query<iFlyChat_Site>("select Id, Name, ApiKey, RoomId from [Site]").ToList();
            }
        }

        public static void SiteUserUpdate(int id, int uid) {
            using (var conn = new SqlConnection(Config.ConnStr)) {
                if (Config.SitesUsers.Any(x => x.SiteId == id && x.UserId == uid)) {
                    //Delete
                    conn.Query(@"delete from [Site_User] where [SiteId]=@Id and [UserId]=@Uid", new { Id = id, Uid = uid });
                } else {
                    // Save
                    conn.Query(@"insert into [Site_User] ([SiteId], [UserId]) values (@Id, @Uid)", new { Id = id, Uid = uid });
                }
                Config.SitesUsers = conn.Query<iFlyChat_Site_User>("select Id, SiteId, UserId from [Site_User]").ToList();
            }
        }



        public static void DeleteUser(int id) {
            using (var conn = new SqlConnection(Config.ConnStr)) {
                if (Config.Users.Any(x => x.Id == id)) {
                    //Delete
                    conn.Query(@"delete from [User] where [Id]=@Id", new { Id = id });
                }
                Config.Users = conn.Query<iFlyChat_User>("select Id, Name, Active, [Image], SourceId, FlyChatId from [User]").ToList();
            }
        }

        public static void SaveUser(iFlyChat_User user) {
            using (var conn = new SqlConnection(Config.ConnStr)) {
                if (Config.Users.Any(x => x.Id == user.Id)) {
                    //Update
                    conn.Query(
                        @"update [User] set [Name]=@Name, [Active]=@Active, [Image]=@Image, [SourceId]=@SourceId, [FlyChatId]=@FlyChatId where [Id]=@Id",
                        new { Name = user.Name, Active = user.Active, Image = user.Image, SourceId = user.SourceId, FlyChatId = user.FlyChatId, Id = user.Id }
                    );
                } else {
                    // Save
                    conn.Query(
                        @"insert into [User] ([Name], [Active], [Image], [SourceId], [FlyChatId]) values (@Name ,@Active, @Image, @SourceId, @FlyChatId)",
                        new { Name = user.Name, Active = user.Active, Image = user.Image, SourceId = user.SourceId, FlyChatId = user.FlyChatId }
                    );
                }
                Config.Users = conn.Query<iFlyChat_User>("select Id, Name, Active, [Image], SourceId, FlyChatId from [User]").ToList();
            }
        }

        public static void DeleteSource(int id) {
            using (var conn = new SqlConnection(Config.ConnStr)) {
                if (Config.Sources.Any(x => x.Id == id)) {
                    //Delete
                    conn.Query(@"delete from [Source] where [Id]=@Id", new { Id = id });
                }
                Config.Sources = conn.Query<iFlyChat_Source>("select Id, Username, SinceId, LastUpdate, ReaderId from [Source]").ToList();
            }
        }

        public static void SaveSite(iFlyChat_Site site) {
            using (var conn = new SqlConnection(Config.ConnStr)) {
                if (Config.Sites.Any(x => x.Id == site.Id)) {
                    //Update
                    conn.Query(
                        @"update [Site] set [Name]=@Name, [ApiKey]=@ApiKey, [RoomId]=@RoomId where [Id]=@Id",
                        new { Name = site.Name, ApiKey = site.ApiKey, RoomId = site.RoomId, Id = site.Id }
                    );
                } else {
                    // Save
                    conn.Query(
                        @"insert into [Site] ([Name], [ApiKey], [RoomId]) values (@Name ,@ApiKey, @RoomId)",
                        new { Name = site.Name, ApiKey = site.ApiKey, RoomId = site.RoomId }
                    );
                }
                Config.Sites = conn.Query<iFlyChat_Site>("select Id, Name, ApiKey, RoomId from [Site]").ToList();
            }
        }

        public static void SaveSource(iFlyChat_Source source) {
            using (var conn = new SqlConnection(Config.ConnStr)) {
                if (Config.Sources.Any(x => x.Id == source.Id)) {
                    //Update
                    conn.Query(@"update [Source] set [Username]=@Username, [SinceId]=@SinceId, [LastUpdate]=@LastUpdate, [ReaderId]=@ReaderId where [Id]=@Id", 
                        new { Username = source.Username, SinceId = source.SinceId, LastUpdate = source.LastUpdate == DateTime.MinValue ? DateTime.Now : source.LastUpdate, Id = source.Id, ReaderId = source.ReaderId } 
                    );
                } else {
                    // Save
                    conn.Query(@"insert into [Source] ([Username], [SinceId], [LastUpdate], [ReaderId]) values (@Username, @SinceId, @LastUpdate, @ReaderId)",
                        new { Username = source.Username, SinceId = source.SinceId, LastUpdate = source.LastUpdate == DateTime.MinValue ? DateTime.Now : source.LastUpdate, ReaderId = source.ReaderId }
                    );
                }
                Config.Sources = conn.Query<iFlyChat_Source>("select Id, Username, SinceId, LastUpdate, ReaderId from [Source]").ToList();
            }
        }

        public static void SaveReader(iFlyChat_Reader_Twitter reader) {
            using (var conn = new SqlConnection(Config.ConnStr)) {
                if (Config.TwtterReaders.Any(x => x.Id == reader.Id)) {
                    //Update
                    conn.Query(@"update [Reader_Twitter] set [Type]=@Type, [twitter_accessToken]=@twitter_accessToken, [twitter_accessTokenSecret]=@twitter_accessTokenSecret, [twitter_consumerKey]=@twitter_consumerKey, [twitter_consumerSecret]=@twitter_consumerSecret, [Username]=@Username, [Password]=@Password, [LastUpdate]=@LastUpdate where [Id]=@Id", 
                        new {
                            Type = reader.Type, twitter_accessToken = reader.twitter_accessToken, twitter_accessTokenSecret = reader.twitter_accessTokenSecret, twitter_consumerKey = reader.twitter_consumerKey, twitter_consumerSecret = reader.twitter_consumerSecret, Username = reader.Username, Password = reader.Password, LastUpdate = reader.LastUpdate == DateTime.MinValue ? DateTime.Now : reader.LastUpdate, Id = reader.Id
                        }
                    );
                } else {
                    // Save
                    conn.Query(@"insert into [Reader_Twitter] ([Type], [twitter_accessToken], [twitter_accessTokenSecret], [twitter_consumerKey], [twitter_consumerSecret], [Username], [Password], [LastUpdate]) values (@Type, @twitter_accessToken, @twitter_accessTokenSecret, @twitter_consumerKey, @twitter_consumerSecret, @Username, @Password, @LastUpdate)",
                        new {
                            Type = reader.Type, twitter_accessToken = reader.twitter_accessToken, twitter_accessTokenSecret = reader.twitter_accessTokenSecret, twitter_consumerKey = reader.twitter_consumerKey, twitter_consumerSecret = reader.twitter_consumerSecret, Username = reader.Username, Password = reader.Password, LastUpdate = reader.LastUpdate == DateTime.MinValue ? DateTime.Now : reader.LastUpdate
                        }
                    );
                }
                Config.TwtterReaders = conn.Query<iFlyChat_Reader_Twitter>(@"select [Id], [Type], [LastUpdate], [Username], [Password], [twitter_accessToken], [twitter_accessTokenSecret], [twitter_consumerKey], [twitter_consumerSecret] from Reader_Twitter").ToList();
            }
        }

        public static void DeleteReader(int id) {
            using (var conn = new SqlConnection(Config.ConnStr)) {
                if (Config.TwtterReaders.Any(x => x.Id == id)) {
                    conn.Query(@"delete from [Reader_Twitter] where [Id]=@Id", new { Id = id });
                }
                Config.TwtterReaders = conn.Query<iFlyChat_Reader_Twitter>(@"select [Id], [Type], [LastUpdate], [Username], [Password], [twitter_accessToken], [twitter_accessTokenSecret], [twitter_consumerKey], [twitter_consumerSecret] from Reader_Twitter").ToList();
            }
        }


        private void Initialize() {
            using (var conn = new SqlConnection(Config.ConnStr)) {
                Config.Sites = conn.Query<iFlyChat_Site>("select Id, Name, ApiKey, RoomId from [Site]").ToList();
                Config.Users = conn.Query<iFlyChat_User>("select Id, Name, Active, [Image], SourceId, FlyChatId from [User]").ToList();
                Config.Sources = conn.Query<iFlyChat_Source>("select Id, Username, SinceId, LastUpdate, ReaderId from [Source]").ToList();
                Config.SitesUsers = conn.Query<iFlyChat_Site_User>("select Id, SiteId, UserId from [Site_User]").ToList();

                // Fetch tweets for 1 or more twitternames
                Config.TwtterReaders = conn.Query<iFlyChat_Reader_Twitter>(@"select [Id], [Type], [LastUpdate], [Username], [Password], [twitter_accessToken], [twitter_accessTokenSecret], [twitter_consumerKey], [twitter_consumerSecret] from Reader_Twitter").ToList();
                Config.Messages = conn.Query<iFlyChat_Message>(@"SELECT [Id],[SourceId],[ReaderId],[UserId],[Text],[Raw],[CreatedOn],[PostedOn] FROM [Message]").ToList();

            }
        }

        public bool Update() {
            // #1: execute all reader, save messages
            // #2: For each iFlyChatUser, 
            Log.Info("Updating feeds");
            foreach (var r in Config.TwtterReaders) {
                // First verify our request count:
                var requests = new List<iFlyChat_Request>();
                using(var conn = new SqlConnection(Config.ConnStr)) {
                    requests = conn.Query<iFlyChat_Request>(@"SELECT [Id],[ReaderId],[SourceId],[CreatedOn] FROM [Request] where [ReaderId]=@Id and [createdOn] > dateadd(minute, -15, getdate())", new { Id = r.Id }).ToList();
                }
                if (requests.Count > 14) {
                    Log.InfoFormat("{0} is on the limit 15 requests pr 15 minutes!", r.Username);
                    continue;
                }
                Log.InfoFormat("Using twitterhandle: {0}", r.Username);
                var sources = r.GetSources();
                var sourcesNotToCheck = requests.Where(x => x.CreatedOn > DateTime.Now.AddMinutes(-1)).Select(y => y.SourceId).ToArray();
                
                foreach(var s in sources.Where(x => !sourcesNotToCheck.Contains(x.Id))) {

                    Log.InfoFormat("\tGetting feeds for: {0}", s.Username);
                    List<JSONObject> newFeeds = null;
                    if (s.LastUpdate < DateTime.Now.AddMinutes(-1)) {
                        api = new API(r.twitter_accessToken, r.twitter_accessTokenSecret, r.twitter_consumerKey, r.twitter_consumerSecret);
                        var param = new Parameters { { "screen_name", s.Username }, { "count", 1 } };
                        if (s.SinceId != 0)
                            param.Add("since_id", s.SinceId);

                        try {
                            newFeeds = api.Get("statuses/user_timeline.json", param);
                            s.LastUpdate = DateTime.Now;
                            iFlyChat_Request req = new iFlyChat_Request() { ReaderId = r.Id, SourceId = s.Id, Message = "OK" };
                            using(var conn = new SqlConnection(Config.ConnStr)) {
                                conn.Query(@"INSERT INTO [Request] ([ReaderId] ,[SourceId],[Message]) VALUES (@ReaderId, @SourceId, @Message)", new { ReaderId = req.ReaderId, SourceId = req.SourceId, Message = req.Message });
                            }
                        } catch (Exception ex) {
                            Log.ErrorFormat("Request exception: {0}", ex.ToString());
                            iFlyChat_Request req = new iFlyChat_Request() { ReaderId = r.Id, SourceId = s.Id, Message = ex.ToString() };
                            using (var conn = new SqlConnection(Config.ConnStr)) {
                                conn.Query(@"INSERT INTO [Request] ([ReaderId] ,[SourceId],[Message]) VALUES (@ReaderId, @SourceId, @Message)", new { ReaderId = req.ReaderId, SourceId = req.SourceId, Message = req.Message });
                            }
                        }
                        
                    }
                    if (newFeeds != null && newFeeds.Count > 0) {
                        foreach (JSONObject json in newFeeds) {
                            
                            s.SinceId = json.Get<Int64>("id");
                            var text = json.Get("text").ToString();
                            using (var conn = new SqlConnection(Config.ConnStr)) {
                                conn.Query(@"insert into [Message] ([SourceId], [ReaderId], [Text], [Raw], [CreatedOn]) values (@SourceId ,@ReaderId,@Text, @Raw, @CreatedOn)",
                                    new { SourceId = s.Id, ReaderId = r.Id, Text = text, Raw = json.ToString(), CreatedOn = DateTime.Now }
                                );
                                foreach (var u in Config.Users.Where(x => x.SourceId == s.Id)) {
                                    // insert into "Queue for all users utilizing source"
                                    conn.Query(@"insert into [Message] ([SourceId], [ReaderId], [UserId], [Text], [Raw], [CreatedOn]) values (@SourceId ,@ReaderId, @UserId, @Text, @Raw, @CreatedOn)",
                                        new { SourceId = s.Id, ReaderId = r.Id, UserId = u.Id, Text = text, Raw = json.ToString(), CreatedOn = DateTime.Now }
                                    );
                                }
                            }
                            iFlyChatBot.SaveSource(s);
                        }
                    }
                }
                using (var conn = new SqlConnection(Config.ConnStr)) {
                    Config.Messages = conn.Query<iFlyChat_Message>(@"SELECT [Id],[SourceId],[ReaderId],[UserId],[Text],[Raw],[CreatedOn],[PostedOn] FROM [Message]").ToList();
                }
                Log.InfoFormat("Read complete for {0}", r.Username);
            }
            Log.InfoFormat("Read complete. {0} Readers, {1} Sources", Config.TwtterReaders.Count, Config.Sources.Count);
            // Iterate over sources, 
            try {

                PostNewMessages();

            } catch (Exception ex) {
                Log.Error("Error running update", ex);
            }
            return true;
        }

        private void PostNewMessages() {
            Log.Info("Posting new messages to rooms, pr user, pr site");
            foreach(var u in Config.Users.Where(x => x.Active && x.SourceId > 0)) {
                var messages = Config.Messages.Where(x => x.SourceId == u.SourceId && x.UserId == u.Id && x.PostedOn < DateTime.Now.AddYears(-15)).OrderBy(y => y.CreatedOn);
                var siteUsers = Config.SitesUsers.Where(x => x.UserId == u.Id).Select(y => y.SiteId).ToArray();
                var sites = Config.Sites.Where(x => siteUsers.Contains(x.Id));
                foreach(var m in messages) {
                    foreach(var s in sites) {
                        PostToChat(s, u, m);
                    }
                }
            }
        }

        public static void PostFromDashboard(int siteId, int userId, string message) {
            var site = Config.Sites.FirstOrDefault(x => x.Id == siteId);
            var user = Config.Users.FirstOrDefault(x => x.Id == userId);
            PostToChatDash(site, user, message);
            
        }

        public static  string PostToChatDash(iFlyChat_Site site, iFlyChat_User user, string message) {
//            var source = Config.Sources.FirstOrDefault(x => x.Id == message.SourceId);
            var text = message;
            if (text.IndexOf("@") == 0 || text.IndexOf("RT") == 0) return string.Empty;
            if (text.LastIndexOf("http") != -1)
                text = text.Substring(0, text.LastIndexOf("http"));
            using (var client = new HttpClient()) {
                var request = new iFlyChat_Publish() {
                    api_key = site.ApiKey,
//                    uid = user.Id.ToString(), // oops, thanks Drew! :)
                    uid = user.FlyChatId.ToString(),
                    name = user.Name,
                    picture_url = user.Image,
                    profile_url = "javascript:void(0)",
                    message = text,
                    color = "#222222",
                    roles = "0" // No idea what this does!
                };
                var publishUrl = "https://api.iflychat.com/api/1.1/room/{0}/publish";
                var response = client.PostAsync(String.Format(publishUrl, site.RoomId.Trim()),
                        new StringContent(JsonConvert.SerializeObject(request),
                            Encoding.UTF8, "application/json"))
                            .Result;

                if (response.IsSuccessStatusCode) {
                    var res = response.Content.ReadAsStringAsync().Result;
                    Log.Info("Posted: ");
                    Log.InfoFormat("\tSite: {0}", site.Name);
                    Log.InfoFormat("\tUser: {0}", user.Name);
                    Log.InfoFormat("\tTwitter: {0}", user.Name);
                    Log.InfoFormat("\tMessage: {0}", text);
//                    using (var conn = new SqlConnection(Config.ConnStr)) {
//                        conn.Query(@"UPDATE [Message] SET [PostedOn]=@PostedOn where [Id]=@Id", new { PostedOn = DateTime.Now, Id = message.Id });
//                        Config.Messages = conn.Query<iFlyChat_Message>(@"SELECT [Id],[SourceId],[ReaderId],[UserId],[Text],[Raw],[CreatedOn],[PostedOn] FROM [Message]").ToList();
//                    }
                    return res;
                }
            }
            return string.Empty;

        }

        public string PostToChat(iFlyChat_Site site, iFlyChat_User user, iFlyChat_Message message) {
            var source = Config.Sources.FirstOrDefault(x => x.Id == message.SourceId);
            var text = message.Text;
            if (text.IndexOf("@") == 0 || text.IndexOf("RT") == 0) return string.Empty;
            if (text.LastIndexOf("http") != -1)
                text = text.Substring(0, text.LastIndexOf("http"));
            using (var client = new HttpClient()) {
                var request = new iFlyChat_Publish() {
                    api_key = site.ApiKey,
                    //uid = user.Id.ToString(), // oops, thanks Drew! :)
                    uid = user.FlyChatId.ToString(),
                    name = user.Name,
                    //picture_url = profile_image_url,
                    picture_url = user.Image,
                    profile_url = "javascript:void(0)",
                    message = text,
                    color = "#222222",
                    roles = "0" // No idea what this does!
                };
                var publishUrl = "https://api.iflychat.com/api/1.1/room/{0}/publish";
                var response = client.PostAsync(String.Format(publishUrl, site.RoomId.Trim()),
                        new StringContent(JsonConvert.SerializeObject(request),
                            Encoding.UTF8, "application/json"))
                            .Result;

                if (response.IsSuccessStatusCode) {
                    var res = response.Content.ReadAsStringAsync().Result;
                    Log.Info("Posted: ");
                    Log.InfoFormat("\tSite: {0}", site.Name);
                    Log.InfoFormat("\tUser: {0}", user.Name);
                    Log.InfoFormat("\tTwitter: {0}", source.Username);
                    Log.InfoFormat("\tMessage: {0}", text);
                    using(var conn = new SqlConnection(Config.ConnStr)) {
                        conn.Query(@"UPDATE [Message] SET [PostedOn]=@PostedOn where [Id]=@Id", new { PostedOn = DateTime.Now, Id = message.Id });
                        Config.Messages = conn.Query<iFlyChat_Message>(@"SELECT [Id],[SourceId],[ReaderId],[UserId],[Text],[Raw],[CreatedOn],[PostedOn] FROM [Message]").ToList();
                    }
                    return res;
                }
            }
            return string.Empty;

        }

        public string PostToChat_(iFlyChat_Site site, iFlyChat_User user, JSONObject json) { // Class for iFlyChat
            var screen_name = json.Get("user.screen_name").ToString();
            var profile_image_url = json.Get("user.profile_image_url").ToString();
            var text = json.Get("text").ToString();
            if (text.IndexOf("@") == 0 || text.IndexOf("RT") == 0) return string.Empty;
            if (text.LastIndexOf("http") != -1)
                text = text.Substring(0, text.LastIndexOf("http"));
            using (var client = new HttpClient()) {
                var request = new iFlyChat_Publish() {
                    api_key = site.ApiKey,
                    uid = user.Id.ToString(),
                    name = user.Name,
                    //picture_url = profile_image_url,
                    picture_url = user.Image,
                    profile_url = "javascript:void(0)",
                    message = text,
                    color = "#222222",
                    roles = "0" // No idea what this does!
                };
                var publishUrl = "https://api.iflychat.com/api/1.1/room/{0}/publish";
                var response = client.PostAsync(String.Format(publishUrl, site.RoomId.Trim()),
                        new StringContent(JsonConvert.SerializeObject(request),
                            Encoding.UTF8, "application/json"))
                            .Result;

                if (response.IsSuccessStatusCode) {
                    var res = response.Content.ReadAsStringAsync().Result;
                    Log.Info("Posted: ");
                    Log.InfoFormat("\tSite: {0}", site.Name);
                    Log.InfoFormat("\tUser: {0}", user.Name);
                    Log.InfoFormat("\tTwitter: {0}", screen_name);
                    Log.InfoFormat("\tMessage: {0}", text);
                    return res;
                }
            }
            return string.Empty;
        }
    }
}
